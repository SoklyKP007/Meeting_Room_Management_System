package com.example.sokly.retrofitapp;

/**
 * Created by Sokly on 5/8/2017.
 */

public class GitHubRepo {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
