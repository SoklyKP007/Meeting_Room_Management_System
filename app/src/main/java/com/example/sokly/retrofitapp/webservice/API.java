package com.example.sokly.retrofitapp.webservice;


import com.example.sokly.retrofitapp.GitHubRepo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Sokly on 5/8/2017.
 */

public interface API {
    @GET("/users/{user}/repos")
    Call<List<GitHubRepo>> reposForUser(@Path("user") String user);
}
