package com.example.sokly.retrofitapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sokly.retrofitapp.GitHubRepo;
import com.example.sokly.retrofitapp.R;

import java.util.List;

/**
 * Created by Sokly on 5/8/2017.
 */

public class GitHubAdapter extends RecyclerView.Adapter<GitHubAdapter.GitHubViewHolder> {
    List<GitHubRepo> mGitHubRepo;
    Context mContext;

    public GitHubAdapter(Context context, List<GitHubRepo> gitHubRepos){
        mContext = context;
        mGitHubRepo = gitHubRepos;
    }

    @Override
    public GitHubViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_row_card_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        GitHubViewHolder holder = new GitHubViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(GitHubViewHolder holder, int position) {
        holder.tv_repo_name.setText(mGitHubRepo.get(position).getName().toString());
    }

    @Override
    public int getItemCount() {
        return mGitHubRepo.size();
    }


    class GitHubViewHolder extends RecyclerView.ViewHolder{
        public TextView tv_repo_name;

        public GitHubViewHolder(View itemView) {
            super(itemView);
            tv_repo_name = (TextView) itemView.findViewById(R.id.tv_repo_name);
        }
    }
}
